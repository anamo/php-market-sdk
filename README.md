# anamo/php-market-sdk

**This repository contains the PHP SDK that allows you to access the Market Platform from your PHP app.**

## Installation

The Market PHP SDK can be installed with Composer. Include this at the top of your PHP file:

```shell

$ composer require-dev anamo/php-market-sdk

```

## Usage

> **Note:** This version of the Market SDK for PHP requires PHP 7.2 or greater.

Simple GET example of a user's profile.

```php
$ws = new \Market\Webstore([
  'app_id' => '{app-id}',
  'app_secret' => '{app-secret}',
  'default_olympian_version' => '2.7',
  //'default_access_token' => '{access-token}', // optional
]);

// Alternatively you can set up the default_access_token at a later time.
//   $ws->setDefaultAccessToken('{access-token}');

try {
  // Get the \Market\OlympianNodes\OlympianNodeWebstore object for the current webstore.
  // If you provided a 'default_access_token', the '{access-token}' is optional.
  $response = $ws->get('/webstores/demo-webstore-id', '{access-token}');
} catch(\Market\Exceptions\MarketResponseException $e) {
  // When Olympian returns an error
  die('Olympian returned an error: ' . $e->getMessage());
} catch(\Market\Exceptions\MarketSDKException | \Exception $e) {
  // When validation fails or other local issues
  die('Market SDK returned an error: ' . $e->getMessage());
}

$webstore = $response->getOlympianNode();
echo 'Browsing in ' . $webstore->getName();
```
Complete documentation, installation instructions, and examples are available [here](https://bitbucket.org/anamo/php-market-sdk/wiki).


## Tests

1. cURL is a prerequisite for running the tests. Install curl to your terminal.
2. Issue an Access Token for your Webstore.

```
#!ssh

curl -G -v -u {app-id}:{app-secret} https://olympian.anamo.one/<olympian_version>/webstores/demo-webstore-id
```


```
#!ssh

curl -G -v -u {access-token} https://olympian.anamo.one/<olympian_version>/webstores/demo-webstore-id
Enter host password for user '{access-token}'
<enter>
```

## Contributing

For us to accept contributions you will have to first have signed the [Contributor License Agreement](https://developers.anamo.market/opensource/cla). Please see [CONTRIBUTING](https://bitbucket.org/anamo/php-market-sdk/master/CONTRIBUTING.md) for details.


## License

Please see the [license file](https://bitbucket.org/anamo/php-market-sdk/master/LICENSE) for more information.


## Security Vulnerabilities

If you have found a security issue, please contact the maintainers directly at [feedback@anamo.company](mailto:feedback@anamo.company).
