<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Market\Exceptions\MarketSDKException;

class MarketBatchRequest extends MarketRequest implements IteratorAggregate, ArrayAccess
{

	/**
	 * @var array An array of MarketRequest entities to send.
	 */
	protected $requests;

	/**
	 * @var array An array of files to upload.
	 */
	protected $attachedFiles;

	/**
	 * Creates a new Request entity.
	 *
	 * @param FacebookApp|null        $app
	 * @param array                   $requests
	 * @param string|null $accessToken
	 * @param string|null             $graphVersion
	 */
	public function __construct(MarketApp $app = null, array $requests = [], string $accessToken = null, $graphVersion = null)
	{
		parent::__construct($app, $accessToken, 'POST', '', [], null, $graphVersion);
		$this->add($requests);
	}

	/**
	 * Adds a new request to the array.
	 *
	 * @param MarketRequest|array $request
	 * @param string|null|array     $options Array of batch request options e.g. 'name', 'omit_response_on_success'.
	 *                                       If a string is given, it is the value of the 'name' option.
	 *
	 * @return FacebookBatchRequest
	 *
	 * @throws \InvalidArgumentException
	 */
	public function add($request, $options = null): self
	{
		if (is_array($request)) {
			foreach ($request as $key => $req) {
				$this->add($req, $key);
			}
			return $this;
		}
		if (!$request instanceof MarketRequest) {
			throw new \InvalidArgumentException('Argument for add() must be of type array or MarketRequest.');
		}
		if (null === $options) {
			$options = [];
		} elseif (!is_array($options)) {
			$options = ['name' => $options];
		}
		$this->addFallbackDefaults($request);
		// File uploads
		$attachedFiles = $this->extractFileAttachments($request);
		$name = isset($options['name']) ? $options['name'] : null;
		unset($options['name']);
		$requestToAdd = [
			'name' => $name,
			'request' => $request,
			'options' => $options,
			'attached_files' => $attachedFiles
		];
		$this->requests[] = $requestToAdd;
		return $this;
	}

	/**
	 * Ensures that the MarketApp and access token fall back when missing.
	 *
	 * @param MarketRequest $request
	 *
	 * @throws MarketSDKException
	 */
	public function addFallbackDefaults(MarketRequest $request): void
	{
		if (!$request->getApp()) {
			$app = $this->getApp();
			if (!$app) {
				throw new MarketSDKException('Missing MarketApp on MarketRequest and no fallback detected on MarketBatchRequest.');
			}
			$request->setApp($app);
		}
		/*if(!$request->getAccessToken()) {
	$accessToken = $this->getAccessToken();
	if(!$accessToken) {
	throw new MarketSDKException('Missing access token on MarketRequest and no fallback detected on MarketBatchRequest.');
	}
	$request->setAccessToken($accessToken);
	}*/
	}

	/**
	 * Extracts the files from a request.
	 *
	 * @param MarketRequest $request
	 *
	 * @return string|null
	 *
	 * @throws MarketSDKException
	 */
	public function extractFileAttachments(MarketRequest $request): ?string
	{
		if (!$request->containsFileUploads()) {
			return null;
		}
		$files = $request->getFiles();
		$fileNames = [];
		foreach ($files as $file) {
			$fileName = uniqid();
			$this->addFile($fileName, $file);
			$fileNames[] = $fileName;
		}
		$request->resetFiles();
		// @TODO Does Graph support multiple uploads on one endpoint?
		return implode(',', $fileNames);
	}

	/**
	 * Return the MarketRequest entities.
	 *
	 * @return array
	 */
	public function getRequests(): array
	{
		return $this->requests;
	}

	/**
	 * Prepares the requests to be sent as a batch request.
	 */
	public function prepareRequestsForBatch(): void
	{
		$this->validateBatchRequestCount();
		$params = [
			'batch' => $this->convertRequestsToJson(),
			'include_headers' => true
		];
		$this->setParams($params);
	}

	/**
	 * Converts the requests into a JSON(P) string.
	 *
	 * @return string
	 */
	public function convertRequestsToJson(): string
	{
		$requests = [];
		foreach ($this->requests as $request) {
			$options = [];
			if (null !== $request['name']) {
				$options['name'] = $request['name'];
			}
			$options += $request['options'];
			$requests[] = $this->requestEntityToBatchArray($request['request'], $options, $request['attached_files']);
		}
		return json_encode($requests);
	}

	/**
	 * Validate the request count before sending them as a batch.
	 *
	 * @throws MarketSDKException
	 */
	public function validateBatchRequestCount(): void
	{
		$batchCount = count($this->requests);
		if (0 === $batchCount) {
			throw new MarketSDKException('There are no batch requests to send.');
		} elseif ($batchCount > 50) {
			// Per: https://developers.facebook.com/docs/graph-api/making-multiple-requests#limits
			throw new MarketSDKException('You cannot send more than 50 batch requests at a time.');
		}
	}

	/**
	 * Converts a Request entity into an array that is batch-friendly.
	 *
	 * @param MarketRequest   $request       The request entity to convert.
	 * @param string|null|array $options       Array of batch request options e.g. 'name', 'omit_response_on_success'.
	 *                                         If a string is given, it is the value of the 'name' option.
	 * @param string|null       $attachedFiles Names of files associated with the request.
	 *
	 * @return array
	 */
	public function requestEntityToBatchArray(MarketRequest $request, $options = null, $attachedFiles = null): array
	{
		if (null === $options) {
			$options = [];
		} elseif (!is_array($options)) {
			$options = ['name' => $options];
		}
		$compiledHeaders = [];
		$headers = $request->getHeaders();
		foreach ($headers as $name => $value) {
			$compiledHeaders[] = $name.': '.$value;
		}
		$batch = [
			'headers' => $compiledHeaders,
			'method' => $request->getMethod(),
			'relative_url' => $request->getUrl()
		];
		// Since file uploads are moved to the root request of a batch request,
		// the child requests will always be URL-encoded.
		$body = $request->getUrlEncodedBody()->getBody();
		if ($body) {
			$batch['body'] = $body;
		}
		$batch += $options;
		if (null !== $attachedFiles) {
			$batch['attached_files'] = $attachedFiles;
		}
		return $batch;
	}

	public function getIterator(): ArrayIterator
	{
		return new ArrayIterator($this->requests);
	}

	public function offsetSet($offset, $value): void
	{
		$this->add($value, $offset);
	}

	public function offsetExists($offset): bool
	{
		return isset($this->requests[$offset]);
	}

	public function offsetUnset($offset): void
	{
		unset($this->requests[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->requests[$offset]) ? $this->requests[$offset] : null;
	}
}
