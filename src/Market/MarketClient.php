<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */
namespace Market;

use Market\HttpClients\MarketCurlHttpClient;
use Market\HttpClients\MarketHttpClientInterface;
use Market\HttpClients\MarketStreamHttpClient;

class MarketClient
{
	const BASE_OLYMPIAN_URL = 'olympian.anamo.one';
	const BASE_OLYMPIAN_URL_BETA = 'beta.olympian.anamo.one';
	const BASE_OLYMPIAN_VIDEO_URL = 'video.olympian.anamo.one';
	const BASE_OLYMPIAN_VIDEO_URL_BETA = 'video.beta.olympian.anamo.one';
	const DEFAULT_REQUEST_TIMEOUT = 240;
	const DEFAULT_FILE_UPLOAD_REQUEST_TIMEOUT = 3600;
	protected $overrideOlympianHost;
	protected $enableBetaMode = false;
	protected $httpClientHandler;
	public static $requestCount = 0;

	public function __construct(MarketHttpClientInterface $httpClientHandler = null, bool $enableBeta = false, ?string $overrideOlympianHost = null)
	{
		$this->httpClientHandler = $httpClientHandler ?: $this->detectHttpClientHandler();
		$this->overrideOlympianHost = $overrideOlympianHost;
		$this->enableBetaMode = $enableBeta;
	}

	public function setHttpClientHandler(MarketHttpClientInterface $httpClientHandler): void
	{
		$this->httpClientHandler = $httpClientHandler;
	}

	public function getHttpClientHandler(): MarketHttpClientInterface
	{
		return $this->httpClientHandler;
	}

	public function detectHttpClientHandler(): MarketHttpClientInterface
	{
		return extension_loaded('curl') ? new MarketCurlHttpClient : new MarketStreamHttpClient;
	}

	public function enableBetaMode(bool $betaMode = true): void
	{
		$this->enableBetaMode = $betaMode;
	}

	public function getBaseOlympianHost(bool $postToVideoUrl = false): string
	{
		if (!empty($this->overrideOlympianHost)) {
			return $this->overrideOlympianHost;
		}
		if ($postToVideoUrl) {
			return $this->enableBetaMode ? static::BASE_OLYMPIAN_VIDEO_URL_BETA : static::BASE_OLYMPIAN_VIDEO_URL;
		}
		return $this->enableBetaMode ? static::BASE_OLYMPIAN_URL_BETA : static::BASE_OLYMPIAN_URL;
	}

	public function getBaseOlympianUrl(bool $postToVideoUrl = false): string
	{
		return getenv('HTTPS_PROXY') ?? 'https://'.$this->getBaseOlympianHost($postToVideoUrl);
	}

	public function prepareRequestMessage(MarketRequest $request): array
	{
		$postToVideoUrl = $request->containsVideoUploads();
		$url = $this->getBaseOlympianUrl($postToVideoUrl).$request->getUrl();
		$host = $this->getBaseOlympianHost($postToVideoUrl);
		// If we're sending files they should be sent as multipart/form-data
		if ($request->containsFileUploads()) {
			$requestBody = $request->getMultipartBody();
			$request->setHeaders([
				'Content-Type' => 'multipart/form-data; boundary='.$requestBody->getBoundary(),
				'Host' => $host
			]);
		} elseif (in_array($request->getMethod(), ['POST', 'PATCH'])) {
			$requestBody = $request->getJsonEncodedBody();
			$request->setHeaders([
				'Content-Type' => 'application/vnd.api+json; charset=UTF-8',
				'Accept' => 'application/vnd.api+json',
				'Host' => $host
			]);
		} else {
			$requestBody = $request->getUrlEncodedBody();
			$request->setHeaders([
				'Content-Type' => 'application/x-www-form-urlencoded',
				'Accept' => 'application/vnd.api+json',
				'Host' => $host
			]);
		}
		return [
			$url,
			$request->getMethod(),
			$request->getHeaders(),
			$requestBody->getBody(),
			!empty($request->getAccessToken()) ? $request->getAccessToken() : $request->getApp()->serialize()
		];
	}

	public function sendRequest(MarketRequest $request): MarketResponse
	{
		list($url, $method, $headers, $body, $pwd) = $this->prepareRequestMessage($request);
		// Since file uploads can take a while, we need to give more time for uploads
		$timeOut = $request->containsFileUploads() ? static::DEFAULT_FILE_UPLOAD_REQUEST_TIMEOUT : static::DEFAULT_REQUEST_TIMEOUT;
		// Should throw `MarketSDKException` exception on HTTP client error.
		// Don't catch to allow it to bubble up.
		$rawResponse = $this->httpClientHandler->send($url, $method, $body, $pwd, $headers, $timeOut);
		static::$requestCount++;
		$returnResponse = new MarketResponse(
			$request,
			$rawResponse->getBody(),
			$rawResponse->getHttpResponseCode(),
			$rawResponse->getHeaders()
		);
		if ($returnResponse->isError()) {
			throw $returnResponse->getThrownException();
		}
		return $returnResponse;
	}

	public function sendBatchRequest(MarketBatchRequest $request): MarketBatchResponse
	{
		$request->prepareRequestsForBatch();
		$timeOut = $request->containsFileUploads() ? static::DEFAULT_FILE_UPLOAD_REQUEST_TIMEOUT : static::DEFAULT_REQUEST_TIMEOUT;
		$batch = [];
		foreach ($request->getRequests() as $req) {
			$req['request'] = $this->prepareRequestMessage($req['request']);
			$batch[] = $req['request'];
		}
		$rawResponses = $this->httpClientHandler->batch($batch, $timeOut);
		static::$requestCount += count($rawResponses);
		return new MarketBatchResponse($request, $rawResponses);
	}
}
