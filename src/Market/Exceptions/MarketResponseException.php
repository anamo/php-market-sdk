<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\Exceptions;

use Market\MarketResponse;

class MarketResponseException extends MarketSDKException
{
	protected $response;

	protected $responseData;

	public function __construct(MarketResponse $response, MarketSDKException $previousException = null)
	{
		$this->response = $response;
		$this->responseData = $response->getDecodedBody();
		$errorMessage = $this->get('title', 'Unknown error from Olympian.');
		$errorCode = $this->get('status', -1);
		parent::__construct($errorMessage, $errorCode, $previousException);
	}

	public static function create(MarketResponse $response): MarketResponseException
	{
		$data = $response->getDecodedBody();
		$code = reset($data['errors'])['status'] ?? null;
		$message = $data['errors']['title'] ?? 'Unknown error from Olympian.';
		switch ($code) {
			// auth stuff
			case 401:
			case 423:
				return new static($response, new MarketAuthenticationException($message, $code));
			// Server issue, possible downtime
			case 500:
			case 502:
			case 503:
				return new static($response, new MarketServerException($message, $code));
			// API Throttling
			case 4:
			case 17:
			case 341:
				return new static($response, new MarketThrottleException($message, $code));
			// Duplicate Post
			case 506:
				return new static($response, new MarketClientException($message, $code));
			// Video upload resumable error
			case 1363030:
			case 1363019:
			case 1363037:
			case 1363033:
			case 1363021:
			case 1363041:
				return new static($response, new MarketResumableUploadException($message, $code));
		}
		// Missing Permissions
		if (10 == $code || ($code >= 200 && $code <= 299)) {
			return new static($response, new MarketAuthorizationException($message, $code));
		}
		// OAuth authentication error
		/*if(isset($data['error']['type']) && $data['error']['type'] === 'OAuthException') {
		return new static($response, new MarketAuthenticationException($message, $code));
		}*/
		// All others
		return new static($response, new MarketOtherException($message, $code));
	}

	private function get(string $key, $default = null)
	{
		if (isset(reset($this->responseData['errors'])[$key])) {
			return reset($this->responseData['errors'])[$key];
		}
		return $default;
	}

	public function getHttpStatusCode(): int
	{
		return $this->response->getHttpStatusCode();
	}

	public function getDetail(): int
	{
		return $this->get('detail');
	}

	public function getErrorType(): string
	{
		return $this->get('type', '');
	}

	public function getRawResponse(): string
	{
		return $this->response->getBody();
	}

	public function getResponseData(): array
	{
		return $this->responseData;
	}

	public function getResponse(): MarketResponse
	{
		return $this->response;
	}
}
