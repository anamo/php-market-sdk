<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use Market\Exceptions\MarketSDKException;

class MarketApp implements \Serializable
{
	protected $id;
	protected $secret;

	/**
	 * @throws MarketSDKException
	 *
	>	$obj = new MarketApp;
	 *
	 */
	public function __construct(string $id, string $secret)
	{
		if (!is_string($id)
			// Keeping this for BC. Integers greater than PHP_INT_MAX will make is_int() return false
			 && !is_int($id)) {
			throw new MarketSDKException('The "app_id" must be formatted as a string since many app ID\'s are greater than PHP_INT_MAX on some systems.');
		}
		// We cast as a string in case a valid int was set on a 64-bit system and this is unserialised on a 32-bit system
		$this->id = (string) $id;
		$this->secret = $secret;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getSecret(): string
	{
		return $this->secret;
	}

	/**
	 * Serializes the MarketApp entity as a string.
	 *
	>	var_dump(serialize((new MarketApp)));
	 *
	 */
	public function serialize(): string
	{
		return implode(':', [$this->id, $this->secret]);
	}

	/**
	 * Unserializes a string as a MarketApp entity.
	 *
	>	var_dump(unserialize((string)(new MarketApp)));
	 *
	 */
	public function unserialize($serialized_data): void
	{
		list($id, $secret) = explode(':', $serialized_data);
		$this->__construct($id, $secret);
	}
}
