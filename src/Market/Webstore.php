<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use Market\Exceptions\MarketSDKException;
use Market\HttpClients\HttpClientsFactory;
use Market\PersistentData\PersistentDataFactory;

class Webstore
{
	/**
	 * @const string Version number of the Market PHP SDK.
	 */
	const VERSION = '5.0.0';

	/**
	 * @const string Default Olympian API version for requests.
	 */
	const DEFAULT_OLYMPIAN_VERSION = '2.8';

	/**
	 * @const string The name of the environment variable that contains the app ID.
	 */
	const APP_ID_ENV_NAME = 'access_key_id';

	/**
	 * @const string The name of the environment variable that contains the app secret.
	 */
	const APP_SECRET_ENV_NAME = 'access_key_secret';

	/**
	 * @const string The name of the environment variable that contains the olympian version.
	 */
	const OLYMPIAN_VERSION_ENV_NAME = 'olympian_version';

	/**
	 * @var MarketApp The MarketApp entity.
	 */
	protected $app;

	protected $client;

	protected $defaultAccessToken;

	protected $defaultOlympianVersion;

	protected $persistentDataHandler;

	protected $lastResponse;

	public function __construct(array $config = [])
	{
		$config = array_merge([
			'app_id' => getenv(static::APP_ID_ENV_NAME),
			'app_secret' => getenv(static::APP_SECRET_ENV_NAME),
			'default_olympian_version' => getenv(static::OLYMPIAN_VERSION_ENV_NAME),
			'enable_beta_mode' => false,
			'http_client_handler' => null,
			'persistent_data_handler' => null
		], $config);
		if (!$config['app_id']) {
			throw new MarketSDKException('Required "app_id" key not supplied in config and could not find fallback environment variable "'.static::APP_ID_ENV_NAME.'"');
		}
		if (!$config['app_secret']) {
			throw new MarketSDKException('Required "app_secret" key not supplied in config and could not find fallback environment variable "'.static::APP_SECRET_ENV_NAME.'"');
		}
		//if(!$config['default_olympian_version']) {
		//throw new \InvalidArgumentException('Required "default_olympian_version" key not supplied in config');
		//}
		$this->app = new MarketApp($config['app_id'], $config['app_secret']);
		$this->client = new MarketClient(
			HttpClientsFactory::createHttpClient($config['http_client_handler']),
			$config['enable_beta_mode'],
			$config['override_olympian_host']
		);
		$this->persistentDataHandler = PersistentDataFactory::createPersistentDataHandler(
			$config['persistent_data_handler']
		);
		if (isset($config['default_access_token'])) {
			$this->setDefaultAccessToken($config['default_access_token']);
		}
		$this->defaultOlympianVersion = $config['default_olympian_version'];
	}

	public function getApp(): MarketApp
	{
		return $this->app;
	}

	public function getClient(): MarketClient
	{
		return $this->client;
	}

	public function getLastResponse(): MarketResponse
	{
		return $this->lastResponse;
	}

	public function getDefaultAccessToken(): string
	{
		return $this->defaultAccessToken;
	}

	public function setDefaultAccessToken(string $accessToken)
	{
		if (!is_string($accessToken)) {
			throw new \InvalidArgumentException('The default access token must be of type "string"');
		}
		$this->defaultAccessToken = $accessToken;
	}

	public function getDefaultOlympianVersion(): string
	{
		return $this->defaultOlympianVersion;
	}

	/**
	 * Sends a GET request to Olympian and returns the result.
	 */
	public function get(string $endpoint, string $accessToken = null, string $eTag = null, string $olympianVersion = null): MarketResponse
	{
		return $this->sendRequest(
			'GET',
			$endpoint,
			$params = [],
			$accessToken,
			$eTag,
			$olympianVersion
		);
	}

	/**
	 * Sends a POST request to Olympian and returns the result.
	 */
	public function post(string $endpoint, array $params = [], string $accessToken = null, string $eTag = null, string $olympianVersion = null): MarketResponse
	{
		return $this->sendRequest(
			'POST',
			$endpoint,
			$params,
			$accessToken,
			$eTag,
			$olympianVersion
		);
	}

	/**
	 * Sends a DELETE request to Olympian and returns the result.
	 */
	public function delete(string $endpoint, array $params = [], string $accessToken = null, string $eTag = null, string $olympianVersion = null): MarketResponse
	{
		return $this->sendRequest(
			'DELETE',
			$endpoint,
			$params,
			$accessToken,
			$eTag,
			$olympianVersion
		);
	}

	/**
	 * Sends a request to Olympian and returns the result.
	 */
	public function sendRequest(string $method, string $endpoint, array $params = [], string $accessToken = null, string $eTag = null, string $olympianVersion = null): MarketResponse
	{
		$olympianVersion = $olympianVersion ?: $this->defaultOlympianVersion;
		$request = $this->request($method, $endpoint, $params, $accessToken, $eTag, $olympianVersion);
		return $this->lastResponse = $this->client->sendRequest($request);
	}

	/**
	 * Sends a batched request to Olympian and returns the result.
	 */
	public function sendBatchRequest(array $requests, string $accessToken = null, string $olympianVersion = null): MarketBatchResponse
	{
		$accessToken = $accessToken ?: $this->defaultAccessToken;
		$olympianVersion = $olympianVersion ?: $this->defaultOlympianVersion;
		$batchRequest = new MarketBatchRequest(
			$this->app,
			$requests,
			$accessToken,
			$olympianVersion
		);
		return $this->lastResponse = $this->client->sendBatchRequest($batchRequest);
	}

	/**
	 * Instantiates a new MarketRequest entity.
	 */
	public function request(string $method, string $endpoint, array $params = [], string $accessToken = null, string $eTag = null, string $olympianVersion = null): MarketRequest
	{
		$accessToken = $accessToken ?: $this->defaultAccessToken;
		$olympianVersion = $olympianVersion ?: $this->defaultOlympianVersion;
		return new MarketRequest(
			$this->app,
			$accessToken,
			$method,
			$endpoint,
			$params,
			$eTag,
			$olympianVersion
		);
	}
}
