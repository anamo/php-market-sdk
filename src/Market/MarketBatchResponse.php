<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;

class MarketBatchResponse extends MarketResponse implements IteratorAggregate, ArrayAccess
{
	protected $batchRequest;
	protected $responses = [];

	public function __construct(MarketBatchRequest $batchRequest, array $responses)
	{
		$this->batchRequest = $batchRequest;

		$this->setResponses($responses);
	}

	public function getResponses(): array
	{
		return $this->responses;
	}

	public function setResponses(array $responses): void
	{
		$this->responses = [];
		foreach ($responses as $key => $olympianResponse) {
			$this->addResponse($key, $olympianResponse);
		}
	}

	public function addResponse($key, $response): void
	{
		$originalRequestName = $this->batchRequest[$key]['name'] ?? $key;
		$originalRequest = $this->batchRequest[$key]['request'] ?? null;
		$httpResponseBody = $response->getBody() ?? null;
		$httpResponseCode = $response->getHttpResponseCode() ?? null;
		$httpResponseHeaders = $response->getHeaders();
		// @TODO With PHP 5.5 support, this becomes array_column($response['headers'], 'value', 'name')
		//$httpResponseHeaders = isset($response['headers']) ? $this->normalizeBatchHeaders($response['headers']) : [];
		$this->responses[$originalRequestName] = new MarketResponse(
			$originalRequest,
			$httpResponseBody,
			$httpResponseCode,
			$httpResponseHeaders
		);
	}

	public function getIterator(): ArrayIterator
	{
		return new ArrayIterator($this->responses);
	}

	public function offsetSet($offset, $value): void
	{
		$this->addResponse($offset, $value);
	}

	public function offsetExists($offset): bool
	{
		return isset($this->responses[$offset]);
	}

	public function offsetUnset($offset): void
	{
		unset($this->responses[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->responses[$offset] ?? null;
	}

	private function normalizeBatchHeaders(array $batchHeaders): array
	{
		$headers = [];
		foreach ($batchHeaders as $header) {
			$headers[$header['name']] = $header['value'];
		}
		return $headers;
	}
}
