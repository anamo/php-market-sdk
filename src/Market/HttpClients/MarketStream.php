<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Facebook\HttpClients;

class MarketStream
{
	protected $stream;
	protected $responseHeaders = [];

	public function streamContextCreate(array $options): void
	{
		$this->stream = stream_context_create($options);
	}

	public function getResponseHeaders(): array
	{
		return $this->responseHeaders;
	}

	public function fileGetContents(string $url)
	{
		$rawResponse = file_get_contents($url, false, $this->stream);
		$this->responseHeaders = $http_response_header ?: [];
		return $rawResponse;
	}
}
