<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\HttpClients;

use Exception;
use InvalidArgumentException;

class HttpClientsFactory
{
	private function __construct()
	{
		// a factory constructor should never be invoked
	}

	/**
	 * HTTP client generation.
	 */
	public static function createHttpClient($handler): MarketHttpClientInterface
	{
		if (!$handler) {
			return self::detectDefaultClient();
		}
		if ($handler instanceof MarketHttpClientInterface) {
			return $handler;
		}
		if ('stream' === $handler) {
			return new MarketStreamHttpClient;
		}
		if ('curl' === $handler) {
			if (!extension_loaded('curl')) {
				throw new Exception('The cURL extension must be loaded in order to use the "curl" handler.');
			}
			return new MarketCurlHttpClient;
		}
		/*if('guzzle' === $handler && !class_exists('GuzzleHttp\Client')) {
		throw new Exception('The Guzzle HTTP client must be included in order to use the "guzzle" handler.');
		}
		if($handler instanceof Client) {
		return new MarketGuzzleHttpClient($handler);
		}
		if('guzzle' === $handler) {
		return new MarketGuzzleHttpClient;
		}*/
		throw new InvalidArgumentException('The http client handler must be set to "curl", "stream", "guzzle", be an instance of GuzzleHttp\Client or an instance of Market\HttpClients\MarketHttpClientInterface');
	}

	/**
	 * Detect default HTTP client.
	 */
	private static function detectDefaultClient(): MarketHttpClientInterface
	{
		if (extension_loaded('curl')) {
			return new MarketCurlHttpClient;
		}
		if (class_exists('GuzzleHttp\Client')) {
			return new MarketGuzzleHttpClient;
		}
		return new MarketStreamHttpClient;
	}
}
