<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\HttpClients;

use Market\Exceptions\MarketSDKException;
use Market\Http\OlympianRawResponse;

class MarketCurlHttpClient implements MarketHttpClientInterface
{
	protected $curlErrorMessage = '';
	protected $curlErrorCode = 0;
	protected $rawResponse;
	protected $marketCurl;

	public function __construct()
	{}

	public function send(string $url, string $method, string $body, ?string $pwd, array $headers, int $timeOut)
	{
		$this->openConnection($url, $method, $body, $pwd, $headers, $timeOut);
		$this->sendRequest();
		if ($this->curlErrorCode = curl_errno($this->marketCurl)) {
			throw new MarketSDKException(curl_error($this->marketCurl), $this->curlErrorCode);
		}
		// Separate the raw headers from the raw body
		list($rawHeaders, $rawBody) = $this->extractResponseHeadersAndBody();
		$this->closeConnection();
		return new OlympianRawResponse($rawHeaders, $rawBody);
	}

	public function batch(array $batch, int $timeOut): array
	{
		$mh = curl_multi_init();

		$handles = [];
		foreach ($batch as $voley) {
			list($url, $method, $headers, $body, $pwd) = $voley;
			$this->openConnection($url, $method, $body, $pwd, $headers, $timeOut);
			curl_multi_add_handle($mh, $this->marketCurl);
			$handles[] = $this->marketCurl;
		}

		$this->marketCurl = $mh;

		$running = null;
		do {
			curl_multi_exec($mh, $running);
			curl_multi_select($mh);
		} while ($running > 0);

		if ($this->curlErrorCode = curl_multi_errno($this->marketCurl)) {
			throw new MarketSDKException(curl_error($this->marketCurl), $this->curlErrorCode);
		}

		$resps = [];
		foreach ($handles as $ch) {
			$this->rawResponse = trim(curl_multi_getcontent($ch));
			curl_multi_remove_handle($this->marketCurl, $ch);
			list($rawHeaders, $rawBody) = $this->extractResponseHeadersAndBody();
			$resps[] = new OlympianRawResponse($rawHeaders, $rawBody);
		}
		curl_multi_close($this->marketCurl);

		return $resps;
	}

	public function openConnection(string $url, string $method, string $body, ?string $pwd, array $headers, int $timeOut)
	{
		$options = [
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_HTTPHEADER => $this->compileRequestHeaders($headers),
			CURLOPT_URL => $url,
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_TIMEOUT => $timeOut,
			CURLOPT_RETURNTRANSFER => 1, // Follow 301 redirects
			CURLOPT_HEADER => 1, // Enable header processing
			CURLOPT_USERPWD => $pwd,
			CURLOPT_ENCODING => 'gzip',
			CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
			//CURLOPT_SSL_VERIFYHOST => 2,
			//CURLOPT_SSL_VERIFYPEER => true,
			//CURLOPT_CAINFO => __DIR__ . '/certs/DigiCertHighAssuranceEVRootCA.pem',
		];
		if ('GET' !== $method) {
			$options[CURLOPT_POSTFIELDS] = $body;
		}
		curl_setopt_array($this->marketCurl = curl_init(), $options);
	}

	public function closeConnection(): void
	{
		curl_close($this->marketCurl);
	}

	public function sendRequest(): void
	{
		$this->rawResponse = trim(curl_exec($this->marketCurl));
	}

	public function compileRequestHeaders(array $headers): array
	{
		$return = [];
		foreach ($headers as $key => $value) {
			$return[] = $key.': '.$value;
		}
		return $return;
	}

	public function extractResponseHeadersAndBody(): array
	{
		$parts = explode("\r\n\r\n", $this->rawResponse);
		$rawHeaders = array_shift($parts);
		$rawBody = implode("\r\n\r\n", $parts);
		return [trim($rawHeaders), trim($rawBody)];
	}
}
