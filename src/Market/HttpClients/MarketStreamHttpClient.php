<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\HttpClients;

use Market\Exceptions\MarketSDKException;
use Market\Http\OlympianRawResponse;

class MarketStreamHttpClient implements MarketHttpClientInterface
{
	protected $marketStream;

	public function __construct(MarketStream $marketStream = null)
	{
		$this->marketStream = $marketStream ?: new MarketStream();
	}

	public function send(string $url, string $method, string $body, ?string $pwd, array $headers, int $timeOut)
	{
		$options = [
			'http' => [
				'method' => $method,
				'header' => $this->compileHeader($headers),
				'content' => $body,
				'timeout' => $timeOut,
				'ignore_errors' => true
			],
			'ssl' => [
				'verify_peer' => true,
				'verify_peer_name' => true,
				'allow_self_signed' => true, // All root certificates are self-signed
				'cafile' => __DIR__.'/certs/DigiCertHighAssuranceEVRootCA.pem'
			]
		];
		$this->marketStream->streamContextCreate($options);
		$rawBody = $this->marketStream->fileGetContents($url);
		$rawHeaders = $this->marketStream->getResponseHeaders();
		if (false === $rawBody || empty($rawHeaders)) {
			throw new MarketSDKException('Stream returned an empty response', 660);
		}
		$rawHeaders = implode("\r\n", $rawHeaders);
		return new OlympianRawResponse($rawHeaders, $rawBody);
	}

	public function compileHeader(array $headers): string
	{
		$header = [];
		foreach ($headers as $k => $v) {
			$header[] = $k.': '.$v;
		}
		return implode("\r\n", $header);
	}
}
