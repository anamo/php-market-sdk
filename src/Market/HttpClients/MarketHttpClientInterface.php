<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\HttpClients;

interface MarketHttpClientInterface
{
	public function send(string $url, string $method, string $body, ?string $pwd, array $headers, int $timeOut);
}
