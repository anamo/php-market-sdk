<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use Market\Exceptions\MarketResponseException;
use Market\OlympianNodes\OlympianNodeFactory;

class MarketResponse
{
	protected $httpStatusCode;
	protected $headers;
	protected $body;
	protected $decodedBody = [];
	protected $request;
	protected $thrownException;

	public function __construct(MarketRequest $request, string $body = null, int $httpStatusCode = null, array $headers = [])
	{
		$this->request = $request;
		$this->body = $body;
		$this->httpStatusCode = $httpStatusCode;
		$this->headers = $headers;
		$this->decodeBody();
	}

	public function getRequest(): MarketRequest
	{
		return $this->request;
	}

	public function getApp(): MarketApp
	{
		return $this->request->getApp();
	}

	public function getAccessToken(): ?string
	{
		return $this->request->getAccessToken();
	}

	public function getHttpStatusCode(): int
	{
		return $this->httpStatusCode;
	}

	public function getHeaders(): array
	{
		return $this->headers;
	}

	public function getBody(): string
	{
		return $this->body;
	}

	public function getDecodedBody(): array
	{
		return $this->decodedBody;
	}

	public function getETag(): ?string
	{
		return $this->headers['ETag'] ?? null;
	}

	public function getOlympianVersion(): ?string
	{
		return $this->headers['Market-API-Version'] ?? null;
	}

	public function isError(): bool
	{
		return isset($this->decodedBody['errors']);
	}

	public function throwException(): void
	{
		throw $this->thrownException;
	}

	public function makeException(): void
	{
		$this->thrownException = MarketResponseException::create($this);
	}

	public function getThrownException(): ?MarketResponseException
	{
		return $this->thrownException;
	}

	public function decodeBody(): void
	{
		$this->decodedBody = json_decode($this->body, true);
		if (!is_array($this->decodedBody)) {
			$this->decodedBody = [];
		}
		if ($this->isError()) {
			$this->makeException();
		}
	}

	public function getOlympianNodeOrCollection()
	{
		return $this->{'getOlympianNode'.(array_key_exists('type', $this->decodedBody['data']) ? '' : 's')}();
	}

	public function getOlympianNode(): OlympianNodes\OlympianNode
	{
		$factory = new OlympianNodeFactory($this);
		return $factory->makeOlympianNode();
	}

	public function getOlympianNodes(): OlympianNodes\Collection
	{
		$factory = new OlympianNodeFactory($this);
		return $factory->makeOlympianNodes();
	}
}
