<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\PersistentData;

interface PersistentDataInterface
{
	public function get(string $key);

	public function set(string $key, $value);
}
