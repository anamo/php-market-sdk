<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\PersistentData;

class MarketMemoryPersistentDataHandler implements PersistentDataInterface
{
	protected $sessionData = [];

	public function get(string $key)
	{
		return $this->sessionData[$key] ?? null;
	}

	public function set(string $key, $value)
	{
		$this->sessionData[$key] = $value;
	}
}
