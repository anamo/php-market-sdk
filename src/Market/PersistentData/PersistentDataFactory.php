<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\PersistentData;

use InvalidArgumentException;

class PersistentDataFactory
{
	private function __construct()
	{
		// a factory constructor should never be invoked
	}

	public static function createPersistentDataHandler($handler): PersistentDataInterface
	{
		if (!$handler) {
			return new MarketMemoryPersistentDataHandler;
		}
		if ($handler instanceof PersistentDataInterface) {
			return $handler;
		}
		if ('memory' === $handler) {
			return new MarketMemoryPersistentDataHandler;
		}
		throw new InvalidArgumentException('The persistent data handler must be set to "session", "memory", or be an instance of Market\PersistentData\PersistentDataInterface');
	}
}
