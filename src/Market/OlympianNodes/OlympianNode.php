<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\OlympianNodes;

class OlympianNode
{
	protected $id = null;
	protected $attrs = [];
	protected $rels = [];
	protected $hasManys = [];
	protected $belongsTos = [];

	public function __construct(array $data = [], array &$included = null, array &$lookupTable = null)
	{
		$this->id = $data['id'];
		$this->attrs = $data['attributes'] ?? [];
		$this->rels = $data['relationships'] ?? [];

		if (!empty($included)) {
			if (empty($lookupTable)) {
				$lookupTable = [];
				foreach ($included as $item) {
					$lookupTable[$item['type']][$item['id']] = $item;
				}
			}

			foreach ($this->rels as $k => $v) {
				if (empty($v['data'])) {
					continue;
				}

				if (!array_key_exists('type', $v['data'])) {
					$hasManys = [];

					foreach ($v['data'] as $v2) {
						$subclassName = OlympianNodeFactory::getNodeFactoryTypeFromPluralString($v2['type']) ?: OlympianNodeFactory::BASE_OLYMPIAN_NODE_CLASS;

						if (isset($lookupTable[$v2['type']][$v2['id']])) {
							$hasManys[] = new $subclassName($lookupTable[$v2['type']][$v2['id']], $included, $lookupTable);
						}
					}

					$this->hasManys[$k] = new Collection($hasManys);
				} else {
					$type = $v['data']['type'];
					$id = $v['data']['id'];

					if (isset($lookupTable[$type][$id])) {
						$subclassName = OlympianNodeFactory::getNodeFactoryTypeFromPluralString($type) ?: OlympianNodeFactory::BASE_OLYMPIAN_NODE_CLASS;
						$this->belongsTos[$k] = new $subclassName($lookupTable[$type][$id]);
					}
				}
			}
		}
	}

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getAttr(string $name, $default = null)
	{
		return $this->attrs[$name] ?? $default;
	}

	public function getRel(string $name, $default = null)
	{
		return $this->rels[$name] ?? $default;
	}

	public function getAttrNames(): array
	{
		return array_keys($this->attrs);
	}

	public function hasMany(string $key): Collection
	{
		return $this->hasManys[$key] ?? new Collection;
	}

	public function belongsTo(string $key): ?self
	{
		return $this->belongsTos[$key] ?? null;
	}

	public function asJson($options = 0): string
	{ // as seen in http://jsonapi.org/
		return json_encode($this->getObjectMap() /*[
		'data' => [
		'type' => array_search((new \ReflectionClass($this))->getShortName(), parse_ini_file('objects-resource-identifier.ini', true))?? 'olympian-nodes',
		'id' => $this->id,
		'attributes' => $this->attrs,
		'relationships' => $this->rels,
		]
		]*/, $options);
	}

	public function getObjectMap(): array
	{
		return [
			'data' => [
				'type' => array_search((new \ReflectionClass($this))->getShortName(), parse_ini_file(__DIR__.'/objects-resource-identifier.ini', true)) ?? 'olympian-nodes',
				'id' => $this->id,
				'attributes' => $this->attrs,
				'relationships' => array_merge($this->hasManys, array_map(function ($v) {
					$m = $v->getObjectMap();
					unset($m['data']['attributes'], $m['data']['relationships']);
					return $m;
				}, $this->belongsTos))
			]
		];
	}

	public function set(string $name, $value): void
	{
		if ($value instanceof self) {
			$this->belongsTos[$name] = $value;
		} elseif ('id' === $name) {
			$this->id = $value;
		} else {
			$this->attrs[$name] = $value;
		}
	}

	public function pushObject(self $node): void
	{
		throw new NotImplementedException;
		$this->hasManys[$name][] = $node;
	}
}
