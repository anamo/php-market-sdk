<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\OlympianNodes;

class OlympianNodeProductImage extends OlympianNode
{

	public function getSource(?string $size = null): ?string
	{
		switch ($this->getAttr('type')) {
			case 'iframe':
				$hostname = parse_url($this->getAttr('source'), PHP_URL_HOST);
				if (preg_match('/^([a-z0-9]+[.])*sirv[.]com$/', $hostname, $matches)) {
					return $this->getAttr('source')."?image=72&gif.lossy=5&w=512&h=512";
				}
				return $this->getAttr('source');
			case 'photo':
			default:
				return (!getenv('CDN_PROXY') ? 'https://anamo.azureedge.net/media/' : getenv('CDN_PROXY')).$this->getAttr('source').('photo' == $this->getAttr('type') ? "@$size" : '');
		}
	}

	public function getTag(): ?string
	{
		switch ($this->getAttr('type')) {
			case 'iframe':
				$hostname = parse_url($this->getAttr('source'), PHP_URL_HOST);
				return preg_match('/^([a-z0-9]+[.])*sirv[.]com$/', $hostname, $matches) ? 'img' : 'iframe';
			case 'photo':
			default:
				return 'img';
		}
	}
}
