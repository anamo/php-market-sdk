<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\OlympianNodes;

use Market\Exceptions\MarketSDKException;
use Market\MarketResponse;

class OlympianNodeFactory
{
	const BASE_OLYMPIAN_NODE_CLASS = '\Market\OlympianNodes\OlympianNode';

	const BASE_OLYMPIAN_NODE_PREFIX = '\Market\OlympianNodes\\';

	protected $response;
	protected $decodedBody;

	public function __construct(MarketResponse $response)
	{
		$this->response = $response;
		$this->decodedBody = $response->getDecodedBody();
	}

	/**
	 * Tries to convert a MarketResponse entity into a OlympianNode.
	 *
	 * @throws MarketSDKException
	 */
	public function makeOlympianNode(): OlympianNode
	{
		$this->validateResponseCastableAsOlympianNode();
		return $this->castAsOlympianNode($this->decodedBody);
	}

	/**
	 * Tries to convert a MarketResponse entity into a Collection of OlympianNodes.
	 *
	 * @throws MarketSDKException
	 */
	public function makeOlympianNodes(): Collection
	{
		$this->validateResponseAsArray();
		return $this->castAsOlympianNodes($this->decodedBody);
	}

	/**
	 * Validates the decoded body.
	 *
	 * @throws MarketSDKException
	 */
	public function validateResponseAsArray(): void
	{
		if (!is_array($this->decodedBody['data'])) {
			throw new MarketSDKException('Unable to get response from Olympian as array.', 620);
		}
	}

	/**
	 * Validates that the return data can be cast as a OlympianNode.
	 *
	 * @throws MarketSDKException
	 */
	public function validateResponseCastableAsOlympianNode(): void
	{
		if (!(isset($this->decodedBody['data']) && static::isCastableAsOlympianNode($this->decodedBody['data']))) {
			throw new MarketSDKException(
				'Unable to convert response from Olympian to a OlympianNode because the response looks it\'s empty.', 620
			);
		}
	}

	/**
	 * Safely instantiates a OlympianNode of $subclassName.
	 *
	 * @throws MarketSDKException
	 */
	public function safelyMakeOlympianNode(array $data, ?string $subclassName): OlympianNode
	{
		$subclassName = $subclassName ?: static::BASE_OLYMPIAN_NODE_CLASS;
		static::validateSubclass($subclassName);
		// Remember the parent node ID
		//$parentNodeId = isset($data['id']) ? $data['id'] : NULL;
		$items = $data['data'];
		return new $subclassName($items, $data['included']);
	}

	/**
	 * Safely instantiates a OlympianNode of $subclassName.
	 *
	 * @throws MarketSDKException
	 */
	public function safelyMakeOlympianNodes(array $data, ?string $subclassName): Collection
	{
		$subclassName = $subclassName ?: static::BASE_OLYMPIAN_NODE_CLASS;
		static::validateSubclass($subclassName);
		// Remember the parent node ID
		//$parentNodeId = isset($data['id']) ? $data['id'] : NULL;
		$items = [];
		foreach ($data['data'] as $k => $v) {
			$items[] = new $subclassName($v, $data['included']);
		}
		return new Collection($items);
	}

	public static function getNodeFactoryTypeFromPluralString(string $type = null): ?string
	{
		$orids = parse_ini_file(__DIR__.'/objects-resource-identifier.ini', true);
		if (!array_key_exists($type, $orids)) {
			return null;
		}
		return static::BASE_OLYMPIAN_NODE_PREFIX.$orids[$type];
	}

	/**
	 * Takes an array of values and determines how to cast each node.
	 *
	 * @throws MarketSDKException
	 */
	public function castAsOlympianNode(array $data, string $parentKey = null, string $parentNodeId = null): OlympianNode
	{
		$subclassName = static::getNodeFactoryTypeFromPluralString($data['data']['type']);
		return $this->safelyMakeOlympianNode($data, $subclassName);
	}

	/**
	 * Takes an array of values and determines how to cast each node.
	 *
	 * @throws MarketSDKException
	 */
	public function castAsOlympianNodes(array $data, string $parentKey = null, string $parentNodeId = null): Collection
	{
		$subclassName = static::getNodeFactoryTypeFromPluralString(reset($data['data'])['type']);
		return $this->safelyMakeOlympianNodes($data, $subclassName);
	}

	/**
	 * Determines whether or not the data should be cast as a OlympianEdge.
	 */
	public static function isCastableAsOlympianNode(array $data): bool
	{
		return true;
	}

	/**
	 * Ensures that the subclass in question is valid.
	 *
	 * @throws MarketSDKException
	 */
	public static function validateSubclass(string $subclassName): void
	{
		if (static::BASE_OLYMPIAN_NODE_CLASS == $subclassName || is_subclass_of($subclassName, static::BASE_OLYMPIAN_NODE_CLASS)) {
			return;
		}
		throw new MarketSDKException('The given subclass "'.$subclassName.'" is not valid. Cannot cast to an object that is not a OlympianNode subclass.', 620);
	}
}
