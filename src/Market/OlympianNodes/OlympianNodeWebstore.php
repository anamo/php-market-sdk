<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\OlympianNodes;

class OlympianNodeWebstore extends OlympianNode
{
	public function getName(): ?string
	{
		return $this->getAttr('name');
	}
}
