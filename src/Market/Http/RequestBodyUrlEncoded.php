<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\Http;

class RequestBodyUrlEncoded implements RequestBodyInterface
{
	protected $params = [];

	public function __construct(array $params)
	{
		$this->params = $params;
	}

	public function getBody(): string
	{
		return http_build_query($this->params, null, '&');
	}
}
