<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\Http;

class RequestBodyJsonEncoded implements RequestBodyInterface
{
	protected $params = [];

	public function __construct(array $params)
	{
		$this->params = $params;
	}

	public function getBody(): string
	{
		return json_encode($this->params);
	}
}
