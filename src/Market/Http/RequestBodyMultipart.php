<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\Http;

use Market\FileUpload\MarketFile;

class RequestBodyMultipart implements RequestBodyInterface
{
	private $boundary;
	private $params;
	private $files = [];

	public function __construct(array $params = [], array $files = [], string $boundary = null)
	{
		$this->params = $params;
		$this->files = $files;
		$this->boundary = $boundary ?: uniqid();
	}

	public function getBody(): string
	{
		$body = '';
		// Compile normal params
		$params = $this->getNestedParams($this->params);
		foreach ($params as $k => $v) {
			$body .= $this->getParamString($k, $v);
		}
		// Compile files
		foreach ($this->files as $k => $v) {
			$body .= $this->getFileString($k, $v);
		}
		// Peace out
		$body .= "--{$this->boundary}--\r\n";
		return $body;
	}

	public function getBoundary(): string
	{
		return $this->boundary;
	}

	private function getFileString(string $name, MarketFile $file): string
	{
		return sprintf(
			"--%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"%s\r\n\r\n%s\r\n",
			$this->boundary,
			$name,
			$file->getFileName(),
			$this->getFileHeaders($file),
			$file->getContents()
		);
	}

	private function getParamString(string $name, string $value): string
	{
		return sprintf(
			"--%s\r\nContent-Disposition: form-data; name=\"%s\"\r\n\r\n%s\r\n",
			$this->boundary,
			$name,
			$value
		);
	}

	private function getNestedParams(array $params): array
	{
		$query = http_build_query($params, null, '&');
		$params = explode('&', $query);
		$result = [];
		foreach ($params as $param) {
			list($key, $value) = explode('=', $param, 2);
			$result[urldecode($key)] = urldecode($value);
		}
		return $result;
	}

	protected function getFileHeaders(MarketFile $file): string
	{
		return "\r\nContent-Type: {$file->getMimetype()}";
	}
}
