<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market\Http;

class OlympianRawResponse
{
	protected $headers;

	protected $body;

	protected $httpResponseCode;

	public function __construct($headers, string $body, int $httpStatusCode = null)
	{
		if (is_numeric($httpStatusCode)) {
			$this->httpResponseCode = (int) $httpStatusCode;
		}
		if (is_array($headers)) {
			$this->headers = $headers;
		} else {
			$this->setHeadersFromString($headers);
		}
		$this->body = $body;
	}

	public function getHeaders(): array
	{
		return $this->headers;
	}

	public function getBody(): string
	{
		return $this->body;
	}

	public function getHttpResponseCode(): int
	{
		return $this->httpResponseCode;
	}

	public function setHttpResponseCodeFromHeader(string $rawResponseHeader): void
	{
		preg_match('|HTTP/\d\.\d\s+(\d+)\s+.*|', $rawResponseHeader, $match);
		$this->httpResponseCode = (int) $match[1];
	}

	protected function setHeadersFromString(string $rawHeaders): void
	{
		// Normalize line breaks
		$rawHeaders = str_replace("\r\n", "\n", $rawHeaders);
		// There will be multiple headers if a 301 was followed
		// or a proxy was followed, etc
		$headerCollection = explode("\n\n", trim($rawHeaders));
		// We just want the last response (at the end)
		$rawHeader = array_pop($headerCollection);
		$headerComponents = explode("\n", $rawHeader);
		$this->headers = [];
		foreach ($headerComponents as $line) {
			if (strpos($line, ': ') === false) {
				$this->setHttpResponseCodeFromHeader($line);
			} else {
				list($key, $value) = explode(': ', $line);
				$this->headers[$key] = $value;
			}
		}
	}
}
