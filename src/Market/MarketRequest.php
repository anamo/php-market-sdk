<?php /*! php-market-sdk v2.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-market-sdk */

namespace Market;

use Market\Exceptions\MarketSDKException;
use Market\FileUpload\MarketFile;
use Market\FileUpload\MarketVideo;
use Market\Http\RequestBodyJsonEncoded;
use Market\Http\RequestBodyMultipart;
use Market\Http\RequestBodyUrlEncoded;
use Market\Url\MarketUrlManipulator;

class MarketRequest
{
	protected $app;
	protected $accessToken;
	protected $method;
	protected $endpoint;
	protected $headers = [];
	protected $params = [];
	protected $files = [];
	protected $eTag;
	protected $olympianVersion;

	public function __construct(MarketApp $app = null, string $accessToken = null, string $method = null, string $endpoint = null, array $params = [], string $eTag = null, string $olympianVersion = null)
	{
		$this->setApp($app);
		$this->setAccessToken($accessToken);
		$this->setMethod($method);
		$this->setEndpoint($endpoint);
		$this->setParams($params);
		$this->setETag($eTag);
		$this->olympianVersion = $olympianVersion ?: Webstore::DEFAULT_OLYMPIAN_VERSION;
	}

	public function setAccessToken(?string $accessToken): self
	{
		$this->accessToken = $accessToken;
		return $this;
	}

	/**
	 * Sets the access token with one harvested from a URL or POST params.
	 *
	 */
	public function setAccessTokenFromParams(?string $accessToken): self
	{
		$existingAccessToken = $this->getAccessToken();
		if (!$existingAccessToken) {
			$this->setAccessToken($accessToken);
		} elseif ($accessToken !== $existingAccessToken) {
			throw new MarketSDKException('Access token mismatch. The access token provided in the MarketRequest and the one provided in the URL or POST params do not match.');
		}
		return $this;
	}

	public function getAccessToken(): ?string
	{
		return $this->accessToken;
	}

	public function setApp(?MarketApp $app): void
	{
		$this->app = $app;
	}

	public function getApp(): MarketApp
	{
		return $this->app;
	}

	public function setMethod(string $method)
	{
		$this->method = strtoupper($method);
	}

	public function getMethod(): string
	{
		return $this->method;
	}

	public function validateMethod(): void
	{
		if (!$this->method) {
			throw new MarketSDKException('HTTP method not specified.');
		}
		if (!in_array($this->method, ['GET', 'POST', 'DELETE'])) {
			throw new MarketSDKException('Invalid HTTP method specified.');
		}
	}

	public function setEndpoint(string $endpoint): self
	{
		// Harvest the access token from the endpoint to keep things in sync
		$params = MarketUrlManipulator::getParamsAsArray($endpoint);
		if (isset($params['access_token'])) {
			$this->setAccessTokenFromParams($params['access_token']);
		}
		// Clean the token & app secret proof from the endpoint.
		$filterParams = ['access_token', 'appsecret_proof'];
		$this->endpoint = MarketUrlManipulator::removeParamsFromUrl($endpoint, $filterParams);
		return $this;
	}

	public function getEndpoint(): ?string
	{
		// For batch requests, this will be empty
		return $this->endpoint;
	}

	public function getHeaders(): array
	{
		$headers = static::getDefaultHeaders();
		if ($this->eTag) {
			$headers['If-None-Match'] = $this->eTag;
		}
		return array_merge($this->headers, $headers);
	}

	public function setHeaders(array $headers): void
	{
		$this->headers = array_merge($this->headers, $headers);
	}

	public function setETag(?string $eTag): void
	{
		$this->eTag = $eTag;
	}

	public function setParams(array $params = []): self
	{
		if (isset($params['access_token'])) {
			$this->setAccessTokenFromParams($params['access_token']);
		}
		// Don't let these buggers slip in.
		unset($params['access_token'], $params['appsecret_proof']);
		// @TODO Refactor code above with this
		//$params = $this->sanitizeAuthenticationParams($params);
		$params = $this->sanitizeFileParams($params);
		$this->dangerouslySetParams($params);
		return $this;
	}

	public function dangerouslySetParams(array $params = []): self
	{
		$this->params = array_merge($this->params, $params);
		return $this;
	}

	public function sanitizeFileParams(array $params): array
	{
		foreach ($params as $key => $value) {
			if ($value instanceof MarketFile) {
				$this->addFile($key, $value);
				unset($params[$key]);
			}
		}
		return $params;
	}

	/**
	 * Add a file to be uploaded.
	 *
	 */
	public function addFile(string $key, MarketFile $file): void
	{
		$this->files[$key] = $file;
	}

	/**
	 * Removes all the files from the upload queue.
	 */
	public function resetFiles(): void
	{
		$this->files = [];
	}

	/**
	 * Get the list of files to be uploaded.
	 */
	public function getFiles(): array
	{
		return $this->files;
	}

	/**
	 * Let's us know if there is a file upload with this request.
	 */
	public function containsFileUploads(): bool
	{
		return !empty($this->files);
	}

	/**
	 * Let's us know if there is a video upload with this request.
	 */
	public function containsVideoUploads(): bool
	{
		foreach ($this->files as $file) {
			if ($file instanceof MarketVideo) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the body of the request as multipart/form-data.
	 */
	public function getMultipartBody(): RequestBodyMultipart
	{
		$params = $this->getPostParams();
		return new RequestBodyMultipart($params, $this->files);
	}

	/**
	 * Returns the body of the request as URL-encoded.
	 *
	 */
	public function getUrlEncodedBody(): RequestBodyUrlEncoded
	{
		$params = $this->getPostParams();
		return new RequestBodyUrlEncoded($params);
	}

	/**
	 * Returns the body of the request as JSON-encoded.
	 *
	 */
	public function getJsonEncodedBody(): RequestBodyJsonEncoded
	{
		$params = $this->getPostParams();
		return new RequestBodyJsonEncoded($params);
	}

	/**
	 * Generate and return the params for this request.
	 */
	public function getParams(): array
	{
		return $this->params;
	}

	/**
	 * Only return params on POST requests.
	 */
	public function getPostParams(): array
	{
		if ($this->getMethod() === 'POST') {
			return $this->getParams();
		}
		return [];
	}

	/**
	 * The olympian version used for this request.
	 */
	public function getOlympianVersion(): string
	{
		return $this->olympianVersion;
	}

	/**
	 * Generate and return the URL for this request.
	 */
	public function getUrl(): string
	{
		$this->validateMethod();
		$olympianVersion = MarketUrlManipulator::forceSlashPrefix($this->olympianVersion);
		$endpoint = MarketUrlManipulator::forceSlashPrefix($this->getEndpoint());
		$url = $olympianVersion.$endpoint;
		if ($this->getMethod() !== 'POST') {
			$params = $this->getParams();
			$url = MarketUrlManipulator::appendParamsToUrl($url, $params);
		}
		return $url;
	}

	/**
	 * Return the default headers that every request should use.
	 */
	public static function getDefaultHeaders(): array
	{
		return [
			'User-Agent' => 'mk-php-'.Webstore::VERSION,
			'Accept-Encoding' => '*',
			'REMOTE_ADDR: ' => $_SERVER['REMOTE_ADDR'],
			'HTTP_X_FORWARDED_FOR: ' => $_SERVER['HTTP_X_FORWARDED_FOR']
		];
	}
}
